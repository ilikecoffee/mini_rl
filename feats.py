import numpy as np
import functools
import pandas as pd
# you need to pip install this
from pathos.multiprocessing import ProcessingPool as Pool
import inspect

try:
    import ttools as ss
    import ttools.features.ema as ss_ema
except ImportError:
    import seamless as ss
    import seamless.features.ema as ss_ema

def gen_feats(df, funcs, threads=6):
    def _gen_feats(funcs):
        output = [[as_string(f), f(df)] for f in funcs]
        return output
    if threads == 1:
        feats = _gen_feats(funcs)
    else:
        pool = Pool(nodes=threads)
        chunks = ss.slicing.split_into_chunks(funcs, threads)
        feats = pool.map(_gen_feats, chunks)
        feats = ss.lists.flatten(feats)
    mm = pd.DataFrame({clean_str(col_name):v for col_name, v in feats})
    return mm

def as_string(obj):
    if hasattr(obj, 'func_name'):
        if obj.func_name == '<lambda>':
            representation = inspect.getsource(obj)
        else:
            representation = obj.func_name
    elif isinstance(obj, functools.partial):
        representation = ss.strings._get_partial_representation(obj)
    elif hasattr(obj, '__dict__'):
        representation = ss.strings.get_class_representation(obj)
    else:
        representation = str(obj)
    return representation

def clean_str(string):
    bad_chars = '[, ]<>'
    for c in bad_chars:
        string = string.replace(c, '')
    return string

def get_size_on_book(df, level, side):
    return df['win_{}_size_{}'.format(side, level)]

def get_lagged(x):
    return np.r_[0, x[:-1]]

def lag_output(f):
    def lagged_f(df):
        x = f(df)
        return np.r_[0, x[:-1]]
    return lagged_f

def get_col(df, col):
    return df[col]

def group_apply(f):
    def group_applyed_f(df):
        output = np.repeat(np.nan, len(df))
        for ix in df.get_group_ixs(['win_market_id','selection_id']).values():
            output[ix] = f(df.rowslice(ix))
        return output
    return group_applyed_f


def ema(df, get_col, smoothness, init):
    ids = ss.strings.add_as_strings(df['win_market_id'], df['selection_id'])
    if hasattr(get_col, '__call__'):
        x = get_col(df)
    else:
        x = df[get_col]
    return ss_ema.discrete_group_ema(x, ids, smoothness, init)

def get_first_value(df, col):
    return df[col][0]

