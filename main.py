import pandas as pd
import numpy as np
import xgboost as xgb
from functools import partial

try:
    import ttools as ss
except ImportError:
    import seamless as ss

from mini_rl.feats import (
    get_col, gen_feats, get_size_on_book, lag_output, group_apply, ema, 
    get_first_value
    )

def load_data():
    path = '/code/mini_rl/data/df.h5'
    df = pd.read_hdf(path, 'df')
    df = ss.DDF.from_pandas(df)
    return df

def value_from_policy(df, expected_value_of_policy):
    output = np.repeat(np.nan, len(df))
    for ix in df.get_group_ixs(['win_market_id','selection_id']).values():
        output[ix] = _value_from_policy(df['value_of_cross'][ix], expected_value_of_policy[ix])
    return output

def _value_from_policy(value_of_cross, expected_value_of_policy, min_samples=3, threshold=0.003):
    ix = value_of_cross <= expected_value_of_policy + threshold
    ix[-1] = True
    ix[:min_samples] = False
    output = np.repeat(np.nan, len(value_of_cross))
    output[ix] = value_of_cross[ix]
    output = ss.np.bfill(output)
    return output

if __name__ == '__main__': 

    df = load_data()

    win_market_ids = np.unique(df['win_market_id'])
    split = int(len(win_market_ids) * 0.6)
    train_mids = win_market_ids[:split]
    test_mids = win_market_ids[split:]
    train_ix = np.in1d(df['win_market_id'], train_mids)
    test_ix = np.in1d(df['win_market_id'], test_mids)

    base_funcs = [
        partial(get_col, col='seconds_till_off'),
        ]

    ema_funcs = [
        partial(ema, get_col='value_of_cross', init=0.04, smoothness=100),
        partial(ema, get_col='value_of_cross', init=0.04, smoothness=30),
        partial(ema, get_col='value_of_cross', init=0.04, smoothness=10),
        lambda df: df['benchmark_cross_price']/group_apply(partial(get_first_value, col='benchmark_cross_price'))(df),
        lambda df: ema(df, lambda df:df['benchmark_cross_price']/group_apply(partial(get_first_value, col='benchmark_cross_price'))(df), 3, 0.0),
        lambda df: ema(df, lambda df:df['benchmark_cross_price']/group_apply(partial(get_first_value, col='benchmark_cross_price'))(df), 10, 0.0),
        lambda df: ema(df, lambda df:df['benchmark_cross_price']/group_apply(partial(get_first_value, col='benchmark_cross_price'))(df), 30, 0.0),
        lambda df: ema(df, lambda df:df['benchmark_cross_price']/group_apply(partial(get_first_value, col='benchmark_cross_price'))(df), 100, 0.0),
        lambda df: ema(df, lambda df:df['benchmark_cross_price']/group_apply(partial(get_first_value, col='benchmark_cross_price'))(df), 300, 0.0),

        lambda df: df['value_of_cross']-group_apply(partial(get_first_value, col='value_of_cross'))(df),
        lambda df: ema(df, lambda df:df['value_of_cross']-group_apply(partial(get_first_value, col='value_of_cross'))(df), 3, 0.0),
        lambda df: ema(df, lambda df:df['value_of_cross']-group_apply(partial(get_first_value, col='value_of_cross'))(df), 10, 0.0),
        lambda df: ema(df, lambda df:df['value_of_cross']-group_apply(partial(get_first_value, col='value_of_cross'))(df), 30, 0.0),
        lambda df: ema(df, lambda df:df['value_of_cross']-group_apply(partial(get_first_value, col='value_of_cross'))(df), 100, 0.0),
        lambda df: ema(df, lambda df:df['value_of_cross']-group_apply(partial(get_first_value, col='value_of_cross'))(df), 300, 0.0),
        ]

    funcs = base_funcs + ema_funcs

    mm = gen_feats(df, funcs)

    assert np.isfinite(mm.values.sum(())).all()


    #target_payout = target_size * (target_price -1.)

                          # value_of_cross = f(target_payout, benchamrk_cross_price, cross_size)
    target = df['target'] # function(win_iol_target_payout), benchmark_cross_price, benchmark_cross_size

    train_xgdmat = xgb.DMatrix(mm[train_ix], label=target[train_ix])
    validation_xgdmat = xgb.DMatrix(mm[test_ix], label=target[test_ix])
    watchlist = [(validation_xgdmat, 'test'), (train_xgdmat, 'train')]
    MAX_ITERATIONS = 150
    XGB_PARAMS = {
        'eta': 0.05,
        'max_depth': 10,
        'silent': 1,
        }
    model = xgb.train(XGB_PARAMS, train_xgdmat, MAX_ITERATIONS, watchlist, verbose_eval=True)

    for fold, ix in {'train':train_ix, 'test':test_ix, 'all':np.repeat(True, len(df))}.iteritems():
        print fold
        data = xgb.DMatrix(mm[ix], label=target[ix])
        expected_value_of_policy = model.predict(data)
        actual_value_of_policy = value_from_policy(df[ix], expected_value_of_policy) 
        print 'loss',np.mean(actual_value_of_policy - target[ix])


